using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Multiplier : MonoBehaviour
{
    public int id;
    public int operatorNumber;
    public string operation;
    public GameObject ball;
    int addingTerminator, counter;
    public UIManager manager;
    //List<int> ballsInScene = new List<int>();
    //int ballNumber;
    GameObject[] balls;
    public GameObject spawner;
    bool startCounting;
    // Start is called before the first frame update
    void Start()
    {
        balls = GameObject.FindGameObjectsWithTag("Ball");
        //ballNumber = balls.Length;
        counter = operatorNumber;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            Ball b = other.GetComponent<Ball>();
            if (!b.Verifier(id))
            {
                b.AddID(id);
                startCounting = true;
                CreateBalls(operatorNumber, operation);
            }

        }
         
    }

    void CreateBalls(int number, string oper)
    {
        if (!manager.endGame)
        {
            switch (oper)
            {
                case "+":
                    startCounting = false;
                    //cada pelota que pase va adicio�ndose una m�s hasta alcanzar el n�mero (y cuando lo alcanzo, desaparece)
                    if (addingTerminator < number)
                    {
                        balls = GameObject.FindGameObjectsWithTag("Ball");
                        for (int i = 0; i < number; i++)
                        {
                            GameObject ballClone = Instantiate(ball, RandomPointInBounds(spawner.GetComponent<Collider>().bounds), ball.transform.rotation);
                            //ballNumber++;
                            ballClone.GetComponent<Ball>().AddID(id);
                            ballClone.GetComponent<SphereCollider>().radius = 0.1f;
                        }
                        addingTerminator++;
                        print(addingTerminator);
                        gameObject.GetComponent<Collider>().enabled = false;
                    }
                    break;
                case "-":
                    if (startCounting && counter > 0)
                    {
                        counter--;
                        print("restando y counter es " + counter);
                    }
                    else if (counter == 0)
                    {
                        balls = GameObject.FindGameObjectsWithTag("Ball");
                        print("desbolando");
                        for (int i = 0; i <= number - 1; i++)
                        {
                            Destroy(balls[i]);
                            print("destruyendo");
                        }
                        gameObject.GetComponent<Collider>().enabled = false;
                    }
                    startCounting = false;
                    //cada pelota que pase la voy eliminando hasta que llegue al n�mero total y no elimino m�s
                    break;
                case "x":
                    startCounting = false;
                    for (int i = 0; i < number; i++)
                    {
                        GameObject ballClone = Instantiate(ball, RandomPointInBounds(spawner.GetComponent<Collider>().bounds), ball.transform.rotation);
                        ballClone.GetComponent<Ball>().AddID(id);
                        ballClone.GetComponent<SphereCollider>().radius = 0.1f;
                        StartCoroutine(Kinematiker());
                    }
                    //cada pelota que pase se multiplica por este n�mero
                    break;
                case "/":
                    print("divide");
                    if (startCounting && counter > 0)
                    {
                        counter--;
                    }
                    else if (counter == 0)
                    {
                        balls = GameObject.FindGameObjectsWithTag("Ball");
                        int divisor = (int)balls.Length / number;
                        //tengo un n�mero total de pelotas, cuando haya pasado mi equivalente al operador, divido el total entre ese operador
                        for (int i = 0; i <= divisor; i++)
                        {
                            Destroy(balls[i]);
                            print("destruyendo");
                        }
                        gameObject.GetComponent<Collider>().enabled = false;
                    }
                    startCounting = false;
                    //cuando haya pasado x n�mero de pelotas, quito ese n�mero
                    break;
            }
        }
        
    }
    public /*static*/ Vector3 RandomPointInBounds(Bounds bounds)
    {
        return new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            Random.Range(bounds.min.y, bounds.max.y),
            Random.Range(bounds.min.z, bounds.max.z)
        );
    }

    IEnumerator Kinematiker()
    {
        yield return new WaitForSeconds(0.3f);
        balls = GameObject.FindGameObjectsWithTag("Ball");
        foreach(var ball in balls)
        {
            ball.GetComponent<SphereCollider>().radius = 0.5f;
        }
    }

}
