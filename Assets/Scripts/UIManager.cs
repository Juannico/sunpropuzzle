using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public TMP_Text score, finalScore;
    public TMP_Text time;
    public float timer;
    public Transform finalPos;
    public bool endGame;
    public int glassCounter;
    public GameObject finalContainer;
    // Start is called before the first frame update
    void Start()
    {
        finalContainer.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        score.text = "Pelotas: " + GameObject.FindGameObjectsWithTag("Ball").Length.ToString();
        timer -=  1 * Time.deltaTime;
        time.text = "Tiempo: " + timer.ToString("0");
        if(timer <= 0 || glassCounter >= 4)
        {
            //score.transform.position = finalPos.position;
            finalContainer.SetActive(true);
            finalScore.text = score.text;
            endGame = true;
            time.text = "Tiempo: 0";
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
}
