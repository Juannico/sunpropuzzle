using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gravitron : MonoBehaviour
{
    public float timer;
    //bool starOver;
    public Image[] indicators;
    public ParticleSystem[] particleSystems;
    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(Rotator());
    }

    // Update is called once per frame
    void Update()
    {
        /*if (starOver)
        {
            starOver = false;
            //StartCoroutine(Rotator());
        }*/
    }
    /*IEnumerator Rotator()
    {
        indicator.rectTransform.localEulerAngles = new Vector3(0, 0, -90);
        Physics.gravity = new Vector3(0, -1.0f, 0);
        yield return new WaitForSeconds(timer);
        indicator.rectTransform.localEulerAngles = new Vector3(0, 0, -270);
        Physics.gravity = new Vector3(0, 1.0f, 0);
        yield return new WaitForSeconds(timer);
        indicator.rectTransform.localEulerAngles = new Vector3(0, 0, 0);
        Physics.gravity = new Vector3(1.0f, 0.0f, 0);
        yield return new WaitForSeconds(timer);
        indicator.rectTransform.localEulerAngles = new Vector3(0, 0, -180);
        Physics.gravity = new Vector3(-1.0f, 0.0f, 0);
        yield return new WaitForSeconds(timer);
        starOver = true;
    }*/

    public void GravityBtn(string direction)
    {
        switch (direction)
        {
            case "up":
                foreach(var indicator in indicators)
                {
                    indicator.rectTransform.localEulerAngles = new Vector3(0, 0, -270);
                }
                Physics.gravity = new Vector3(0, 15.0f, 0);
                particleSystems[0].Play();
                particleSystems[1].Stop();
                particleSystems[2].Stop();
                particleSystems[3].Stop();
                break;
            case "down":
                foreach (var indicator in indicators)
                    indicator.rectTransform.localEulerAngles = new Vector3(0, 0, -90);
                Physics.gravity = new Vector3(0, -15.0f, 0);
                particleSystems[0].Stop();
                particleSystems[1].Play();
                particleSystems[2].Stop();
                particleSystems[3].Stop();
                break;
            case "right":
                foreach (var indicator in indicators)
                    indicator.rectTransform.localEulerAngles = new Vector3(0, 0, 0);
                Physics.gravity = new Vector3(15.0f, 0.0f, 0);
                particleSystems[0].Stop();
                particleSystems[1].Stop();
                particleSystems[2].Play();
                particleSystems[3].Stop();
                break;
            case "left":
                foreach (var indicator in indicators)
                    indicator.rectTransform.localEulerAngles = new Vector3(0, 0, -180);
                Physics.gravity = new Vector3(-15.0f, 0.0f, 0);
                particleSystems[0].Stop();
                particleSystems[1].Stop();
                particleSystems[2].Stop();
                particleSystems[3].Play();
                break;
        }
    }
}
