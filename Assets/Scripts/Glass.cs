using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Glass : MonoBehaviour
{
    public TMP_Text glassScoreText;
    int glassScore;
    public int glassGoal;
    public UIManager manager;
    AudioSource ballSound;
    // Start is called before the first frame update
    void Start()
    {
        ballSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        glassScoreText.text = glassScore.ToString() + "/" + glassGoal.ToString();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            ballSound.Play();
            if(glassScore < glassGoal)
            {
                glassScore++;
                Destroy(other.gameObject, 1);
            }
            else
            {
                gameObject.GetComponent<BoxCollider>().isTrigger = false;
                manager.glassCounter++;
            }
        }
    }
}
